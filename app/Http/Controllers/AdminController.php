<?php

namespace App\Http\Controllers;

use App\Models\Optional;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Config;

class AdminController extends Controller
{
    /** Visualizza Utenti
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $utenti = User::all();
        return view('admin.users', compact('utenti'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id)
    {
        $utenti=User::find($id);
        $utenti->username = request()->input('username');
        $utenti->email = request()->input('email');
        $utenti->role = request()->input('role');
        $res=$utenti->update();
        if($res){
            return redirect()->route('admin.user');
        }
    }

    /** Visualizza tutte le configurazioni
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $configs=Config::all();
        $utenti = User::all();
        return view('admin.config', ['configs' => $configs,'utenti'=>$utenti]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $utenti=User::find($id);
        return view('admin.editUser')->with(
            [
                'utenti' => $utenti
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
         $user->delete();
        $config=Config::where('user_id',$user->id );
        $config->delete();
        return  redirect()->route('admin.user');
    }

}
