<?php

namespace App\Http\Controllers;
use App\Models\Config;
use App\Models\Optional;

use App\Models\Prezzi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**Aggiorna le configurazioni nel carrello
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id,Request $request)
    {
        $config = Config::find($id);


        if(isset($_POST['modifica'])) {
            $config->tipoAlimentazione = request()->input('tipoAlimentazione');
            $config->nPorte = request()->input('nPorte');
            $config->tipoCambio = request()->input('tipoCambio');
            $config->user_id = $request->user()->id;


            $res = $config->update();


            if ($res) {
                $optional = new Optional();
                $optional->colore = request()->input('colore');
                $check = request()->input('check');
                if (isset($check)) {
                    $descrizione = implode(" ,", $check);
                    $optional->check = $descrizione;
                }
                $optional->config_id = $config->id;
                $res = $optional->save();
                return redirect()->route('config.cart');
            }
        }
        if (isset($_POST['ordina'])){
            $config=DB::table('config')->where('id',$id)->update(['ordinata'=>1]);

            return redirect()->route('config.cart');

        }
        if(isset($_POS['calcola'])){
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cartEdit($id)
    {
        $config = Config::find($id);
        $optional=Optional::where('config_id',$id)->get();
        $prezzi = Prezzi::all();

        return view('config.order')->with(
            [
                'config' => $config,
                'optional' => $optional,
                'prezzi'=>$prezzi

            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function cartOrder($id)
    {


    }
}
