<?php

namespace App\Http\Controllers;
use App\Models\Album;
use App\Models\Optional;
use App\Models\Config;
use App\Models\User;
use App\Models\Prezzi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;


use Auth;

class ConfigController extends Controller
{
    /**Mostra le configurazioni dell'utente
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $queryBuilder = Config::orderBy('id','DESC');
        $queryBuilder->where('ordinata','=','0');
        $queryBuilder->where('user_id', Auth::user()->id);
        if ($request->has('id')) {
            $queryBuilder->where('id', '=', $request->input('id'));
        }

        $config = $queryBuilder->get();
        return view('config.config', ['config' => $config]);
    }

    /**Crea una nuova configurazione
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

        public function create()
    {

        $config = new Config();

        return view('config.create', ['config' => $config]);

    }


    /**Aggiorna la configurazione
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id,Request $request)
    {
        $config = Config::find($id);
        $optional=Optional::where('config_id',$id );
        $prezzi=Prezzi::all();

        $config->tipoAlimentazione = request()->input('tipoAlimentazione');
        $config->nPorte = request()->input('nPorte');
        $config->tipoCambio = request()->input('tipoCambio');
        $config->stato = request()->input('stato');
        $config->user_id = $request->user()->id;
        $config->updated_at->now();

        $res=$config->update();
        if(isset($_POST['ordina'])){
            $sql = "UPDATE  config SET ordinata=1 WHERE id=".$id."";
            DB::update($sql, ['id' => $id]);

        }


        if($res) {
            $optional = new Optional();
            $optional->colore = request()->input('colore');
            $check=request()->input('check');

            if(isset($check)) {
                $descrizione = implode(" ,", $check);
                $optional->check=$descrizione;
            }
            $optional->config_id = $config->id;
            $res = $optional->save();
            return redirect()->route('config.cart');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $prezzi=Prezzi::all();
        $config = Config::find($id);
        $optional=Optional::where('config_id',$id)->get();

        return view('config.edit')->with(
            [
                'config' => $config,
                'optional' => $optional,
                'prezzi'=>$prezzi,
            ]
        );


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**Elimina la configurazione selezionata
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Config $config)
    {

        $res = $config->delete();
        $optional=Optional::where('config_id',$config->id );
        $optional->delete();
        return redirect()->route('config');



    }

    /**Visualizza tutte le configurazione dell'utente (incluse quelle ordinate)
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function cart(Request $request){


        $queryBuilder = Config::orderBy('id', 'DESC');

        $queryBuilder->where('user_id', Auth::user()->id);
        if ($request->has('id')) {
            $queryBuilder->where('id', '=', $request->input('id'));
        }

        $config = $queryBuilder->get();
        return view('config.cart', ['config' => $config]);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Request $request){


        $request->validate([
            'tipoAlimentazione' => 'required',
            'nPorte' => 'required',
            'tipoCambio'=>'required'
        ]);
        $config=new Config();

        $config->user_id =Auth::id();

        $config->tipoAlimentazione=$request->input('tipoAlimentazione');
        $config->nPorte=$request->input('nPorte');
        $config->tipoCambio=$request->input('tipoCambio');
        $res = $config->save();
         if ($res) {
             $optional = new Optional();
             $optional->colore = $request->input('colore');
              $check=$request->input('check');
              if(isset($check)) {
                  $descrizione = implode(" ,", $check);
                  $optional->check=$descrizione;
              }
              $optional->config_id = $config->id;
              $optional->save();
              return redirect()->route('config');
         }

    }
    public function publicConfig(){

        $query=Config::orderBy('id','DESC');
        $query->where('stato','pubblico');
        $config=$query->get();
        $utenti=User::all();
        return view('config.publicConfig', ['config' => $config,'utenti'=>$utenti]);
    }




}
