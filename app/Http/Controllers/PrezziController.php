<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Config;
use App\Models\Optional;
use App\Models\Prezzi;
use Illuminate\Support\Facades\DB;

class PrezziController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $prezzi=Prezzi::all();

        return view('config.prezzi', ['prezzi' => $prezzi]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id)
    {
        $prezzi = Prezzi::find($id);



       $prezzi->prezzo = request()->input('prezzo');


        $res=$prezzi->update();
        if($res){
            return redirect()->route('admin.Admin');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $prezzi=Prezzi::all();

        return view('admin.Admin', ['prezzi' => $prezzi]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $prezzi=Prezzi::find($id);
        return view('admin.manage', ['prezzi' => $prezzi]);

    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Prezzi $prezzi)
    {
        $prezzi->delete();
        return  redirect()->route('admin.Admin');
    }
}
