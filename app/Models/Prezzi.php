<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Prezzi extends Model
{
    use HasFactory;
    protected $table = 'prezzi';
    protected $primaryKey = 'id';
}
