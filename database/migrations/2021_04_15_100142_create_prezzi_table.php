<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrezziTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prezzi', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->integer('costo');
            $table->integer('benzina');
            $table->integer('elettrico');
            $table->integer('diesel');
            $table->integer('tre');
            $table->integer('cinque');
            $table->integer('manuale');
            $table->integer('automatico');
            $table->integer('colore');
            $table->integer('cerchi');
            $table->integer('vetri');
            $table->integer('fari');
            $table->integer('tetto');
            $table->integer('cruise');
            $table->integer('apple');
            $table->integer('sensori');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
