require('./bootstrap');

require('alpinejs');
function validRegistration(){
    var nome = document.querySelector('#username').value;
    var mail = document.querySelector('#email').value;
    var password = document.querySelector('#password').value;
    var policy = document.querySelector('#policy').checked;

    /*Username validation*/
    if(nome != ''){

        var nomeCaratteri = nome.length;
        var regExpr = /^[a-zA-Z0-9]*$/;

        if(nomeCaratteri > 20){
            document.querySelector('label[for="username"]').innerHTML += '<br><b>Massimo 20 caratteri</b>';
            return false;
        }


    }

    /*email validation*/

    if(mail != ''){
        var regExpr = /^[^@]+@[^@]+\.[^@]+$/;
        if(!regExpr.test(mail)){
            document.querySelector('label[for="email"]').innerHTML += '<br><b>Attenzione: email non  corretta</b>';
            return false;
        }

    }

    /*password validation*/

    if(password != ''){
        var regExpr = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$/!%#*?&])[A-Za-z0-9@/$!%*#?&]{8,}$/;
        if(!regExpr.test(password)){
            document.querySelector('label[for="password"]').innerHTML += '<br><b>Serve almeno: 1 numero,1 maiuscola,1 carattere speciale </b>';
            return false;
        }
    }

    /*policy validation*/
    if(!policy){
        document.querySelector('form').innerHTML += '<br><b>Non hai accettato i termini e le condizioni</b>';

        document.querySelector('#username').value = nome;
        document.querySelector('#email').value = mail;
        document.querySelector('#password').value = password;

        return false;

    }

    return true;
}
