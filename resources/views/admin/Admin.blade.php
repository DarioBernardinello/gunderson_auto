@extends('admin.dashboard')

@section('content')
    <h1 style="text-align:center;">Elenco Prezzi</h1><br><br>
    <form action="" method=post>
        <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">

        <table class="table table-striped table-hover">

                <tr>
                    <td><b>Tipo</b></td>
                    <td><b>Prezzo</b></td>
                    <td><b>Gestisci</b></td>
                </tr>

            @foreach($prezzi as $prezzo)
                <tr>
                    <td>{{$prezzo->campo}}</td>
                    <td>{{$prezzo->prezzo}} &#8364</td>
                    <td><a id="update-button" href="/prezzi/{{$prezzo->id}}/edit" name="update" class="btn btn-success">Update</a></td>
                </tr>

            @endforeach
            <tr style="color:red">
                <td><b>Iva</b></td>
                <td colspan="2"><b>+22%</b></td>
            </tr>
@endsection
