@extends('admin.dashboard')
@section('content')
    <h1 style="text-align:center;">Elenco configurazioni</h1><br><br>
    <form action="" method=post>

        <table class="table table-striped table-hover">
            <tr>
                <td><b>ID</b></td>
                <td><b>Username</b></td>
                <td><b>TipoAlimentazione</b></td>
                <td><b>N. Porte</b></td>
                <td><b>Tipo Cambio</b></td>
                <td><b>Creato</b></td>
                <td><b>Aggiornato</b></td>
                <td><b>Stato</b></td>
                <td><b>Azione</b></td>
            </tr>

            @foreach($configs as $config)
                <tr>
                <td>{{$config->id}}</td>
                    <?php foreach($utenti as $utente){

                             if($utente->id===$config->user_id){
                                echo'<td>'.$utente->username.'</td>';
                             }


                }?>
                <td>{{$config->tipoAlimentazione}}</td>
                <td>{{$config->nPorte}}</td>
                <td>{{$config->tipoCambio}}</td>
                <td>{{$config->created_at}}</td>
                <td>{{$config->updated_at}}</td>
                    <td>{{$config->stato}}</td>
                    <td><a id="delete-button" name="adminConfig" href="{{route('config.destroy',$config->id)}}" class="btn btn-danger">Delete</a></td>
                </tr>
    @endforeach

@endsection
