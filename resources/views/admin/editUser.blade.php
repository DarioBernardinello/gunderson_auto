@extends('admin.dashboard')

@section('content')
    <h1 style="text-align:center">User Edit</h1><br>


    <form action="/users/{{$utenti->id}}" method="POST" style="text-align:center">
        <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
        <input type="hidden" name="_method" value="PATCH">
    <div class="form-group">
        <input type="text"  placeholder="Username" class="form-control" name="username" value="{{$utenti->username}}" required><br>
         <input type="email"  placeholder="Email" class="form-control" name="email" value="{{$utenti->email}}" required><br><br>
        <h5><b>Ruolo</b></h5><br>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="role" id="inlineRadio1" @if($utenti->role=='admin') checked @endif value="admin">
            <label class="form-check-label" for="inlineRadio1">Admin</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="role" id="inlineRadio2"@if($utenti->role=='user') checked @endif value="user">
            <label class="form-check-label" for="inlineRadio2">User</label>
        </div>

    </div>
        <button type="submit" class="btn btn-primary">Salva</button>

    </form>
@endsection
