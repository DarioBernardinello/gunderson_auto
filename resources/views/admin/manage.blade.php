@extends('admin.dashboard')

@section('content')
    <h1>Modifica </h1><br><br>
    <br>

    <form action="/prezzi/{{$prezzi->id}}" method="POST">
        <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
        <input type="hidden" name="_method" value="PATCH">
        <div class="form-group">
            <table class="table">
              <tr>
                 <b><td>ID</td>
                  <td>Campo</td>
                  <td>Prezzo</td></b>
              </tr>
              <tr>
                <td><input type="text"  placeholder="Campo" name="campo" value="{{$prezzi->id}}" disabled readonly></td>
                <td><input type="text"  placeholder="Campo" name="campo" value="{{$prezzi->campo}}" disabled readonly></td>
                <td><input type="number" placeholder="Nuovo Prezzo" name="prezzo" value="{{$prezzi->prezzo}}" required></td>
                <td><button type="submit" class="btn btn-danger" name="salvaprezzo">Salva Prezzo</button></td>
              </tr>

            </table>
        </div>
    </div>
</form>
@endsection
