@extends('admin.dashboard')
@section('content')
    <h1 style="text-align:center;">Elenco Utenti</h1><br><br>
    <form>
        <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
        <table class="table table-striped table-hover">
            <tr>
                <td><b>ID</b></td>
                <td><b>Username</b></td>
                <td><b>Email</b></td>
                <td><b>Data Registrazione</b></td>
                <td><b>Ruolo</b></td>
                <td colspan="2"><b>Gestisci</b></td>
            </tr>
            @foreach($utenti as $utente)
                <tr>
                    <td>{{$utente->id}}</td>
                    <td>{{$utente->username}}</td>
                    <td>{{$utente->email}}</td>
                    <td>{{$utente->created_at}}</td>
                    <td>{{$utente->role}}</td>
                    <td><a id="update-button" href="/users/{{$utente->id}}/edit" name="update" class="btn btn-primary">Update</a>
                        <a id="delete-button" href="{{route('user.destroy',$utente->id)}}"  class="btn btn-danger">Delete</a></td>
                </tr>
    @endforeach
        </table>
    </form>
@endsection
