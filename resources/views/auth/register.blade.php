@extends('templates.default')

@section('content')

<x-guest-layout>

    <x-auth-card>
        <x-slot name="logo">
            <h3>Register</h3>
        </x-slot>

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('register') }}">
            @csrf

            <!-- Name -->
                <div>
                    <x-label for="Username" :value="__('Username')" />

                    <x-input id="username" class="block mt-1 w-full" type="text" name="username" required />
                </div>

            <!-- Email Address -->
            <div class="mt-4">
                <x-label for="email" :value="__('Email')" />

                <x-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required />
            </div>

            <!-- Password -->
            <div class="mt-4">
                <x-label for="password" :value="__('Password')" />

                <x-input id="password" class="block mt-1 w-full"
                                type="password"
                                name="password"
                                required autocomplete="new-password" />
            </div>

            <!-- Confirm Password -->
            <div class="mt-4">
                <x-label for="password_confirmation" :value="__('Conferma Password')" />

                <x-input id="password_confirmation" class="block mt-1 w-full"
                                type="password"
                name="password_confirmation" required />
            </div><br>
                <label for="remember_me" class="inline-flex items-center">
                    <input id="remember_me" type="checkbox" class="rounded border-gray-300 text-indigo-600 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" name="terms">
                    <a href="/termini"><span class="ml-2 text-sm text-gray-600">{{ __('Accetta Termini&Condizioni') }}</span></a>
                </label>
            <div class="flex items-center justify-end mt-4">
                <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                    {{ __('Già registrato?Accedi') }}
                </a>

                <a href="{{route('login')}}"><x-button class="ml-3">
                        {{ __('Registrati') }}
                    </x-button></a>
            </div>
        </form>
    </x-auth-card>
</x-guest-layout>
@endsection
