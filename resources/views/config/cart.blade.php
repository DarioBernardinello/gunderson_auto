@extends('templates.default')

@section('content')

    <form action="">
        <h1 style="text-align:center;">Il mio carrello</h1><br>
        {{-- passiamo il session token per le richieste laravel --}}
        <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">N.Ordine</th>
                <th scope="col">Alimentazione</th>
                <th scope="col">N.Porte</th>
                <th scope="col">Tipo cambio</th>
                <th scope="col">Data Creazione</th>
                <th scope="col">Data Aggiornamento</th>
                <th scope="col">Stato Ordine</th>
                <th  scope="col">Gestisci</th>
                <th></th>

            </tr>
            </thead>

            @foreach ($config as $configs)
                <tr>
                    <td>{{$configs->id}}</td>
                    <td>{{$configs->tipoAlimentazione}}</td>
                    <td>{{$configs->nPorte}}</td>
                    <td> {{$configs->tipoCambio}}</td>
                    <td>{{$configs->created_at}}</td>
                    <td>{{$configs->updated_at}}</td>
                    @if($configs->ordinata==1)
                        <td>Non disponibile</td>
                    <div>
                        <td><button class="btn btn-outline-dark">Ordinata</button>

                    </div>
                    @elseif ($configs->ordinata==0)
                        <td>Disponibile</td>
                        <td><a id="update-button" href="/cart/{{$configs->id}}/edit" class="btn btn-primary">Order</a>
                            <a id="delete-button" href="{{route('config.destroy',$configs->id)}}" class="btn btn-danger">Delete</a></td>

                    @endif

                </tr>
            @endforeach
        </table>
    </form>
@endsection
