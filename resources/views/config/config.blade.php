@extends('templates.default')

@section('content')
    <form>

        <h1 style="text-align:center;">Configurazioni disponibili</h1><br>
        {{-- passiamo il session token per le richieste laravel --}}
        <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">N.Ordine</th>
                <th scope="col">Alimentazione</th>
                <th scope="col">N.Porte</th>
                <th scope="col">Tipo cambio</th>
                <th scope="col">Data Creazione</th>
                <th scope="col">Ultimo Aggiornamento</th>
                <th scope="col">Stato</th>
                <th  scope="col">Gestisci</th>
                <th></th>

            </tr>
            </thead>

            @foreach ($config as $configs)
                <tr>
                    <td>{{$configs->id}}</td>
                    <td>{{$configs->tipoAlimentazione}}</td>
                    <td>{{$configs->nPorte}}</td>
                    <td>{{$configs->tipoCambio}}</td>
                    <td>{{$configs->created_at}}</td>
                    <td>{{$configs->updated_at}}</td>
                    <td>{{$configs->stato}}</td>
                    <div>

                        <td><a id="update-button" href="/config/{{$configs->id}}/edit" class="btn btn-success">Update</a>

                        <a id="delete-button" href="{{route('config.destroy',$configs->id)}}" class="btn btn-danger">Delete</a></td>
                    </div>

                </tr>
            @endforeach
        </table>
        <a id="crea" href="{{route('config.create')}}" class="btn btn-primary">Crea una nuova configurazione</a>

    </form>
@endsection
