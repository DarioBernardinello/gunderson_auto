@extends('templates.default')

@section('content')


    <form action="{{route('config.save')}}" method="POST" style="text-align:center">
        <h1>New Config</h1><br><br>
        @csrf
        <div class="form-group">

            <label for="tipoAlimentazione"><b>*Alimentazione</b></label><br>
            <div class="form-check form-check-inline">
                <input class="form-check-input " type="radio" name="tipoAlimentazione" id="inlineRadio1" value="Benzina">
                <label class="form-check-label" for="inlineRadio1">Benzina</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="tipoAlimentazione" id="inlineRadio2" value="Diesel">
                <label class="form-check-label" for="inlineRadio2">Diesel</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="tipoAlimentazione" id="inlineRadio3" value="Elettrico" >
                <label class="form-check-label" for="inlineRadio3">Elettrico</label>
            </div>

        </div>
        <br>

        <div class="form-group">
            <label for="nPorte"><b>*Numero Porte</b></label><br>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="nPorte" id="inlineRadio1" value="3">
                <label class="form-check-label" for="inlineRadio1">3 </label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="nPorte" id="inlineRadio2" value="5">
                <label class="form-check-label" for="inlineRadio2">5</label>
            </div>

        </div>

        <br>

        <div class="form-group">
            <label for="tipoCambio"><b>*Tipo di Cambio</b></label><br>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="tipoCambio" id="inlineRadio1" value="manuale">
                <label class="form-check-label" for="inlineRadio1">Manuale</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="tipoCambio" id="inlineRadio2" value="automatico">
                <label class="form-check-label" for="inlineRadio2">Automatico</label>
            </div>

        </div><br>

        <h2>Optional</h2><br>
        <p class="form-group">
            <label for="exampleColorInput" class="form-label"><b>Scegli il colore della tua macchina:</b></label>
            <input style="margin-left:auto;margin-right:auto;"type="color" class="form-control form-control-color" name="colore" id="exampleColorInput" value="" title="Choose your color">
            <br>
            <div class="form-check form-switch">
                <div class="col-md">
                    <h4 style="color:red;">Elenco Optional</h4>
                <input class="form-check-input" name="check[]" type="checkbox" value="Cerchi_in_lega" id="flexSwitchCheckDefault">
                <label class="form-check-label" for="flexSwitchCheckDefault"><b>Cerchi in lega</b></label>
                <br>
                <input class="form-check-input" name="check[]" type="checkbox" value="VetriOscurati" id="flexSwitchCheckDefault">
                <label class="form-check-label" for="flexSwitchCheckDefault"><b>Vetri Oscurati</b></label>
                <br>
                <input class="form-check-input" name="check[]" type="checkbox" value="Fari_a_led" id="flexSwitchCheckDefault">
                <label class="form-check-label" for="flexSwitchCheckDefault"><b>Fari full-led</b></label>
                <br>
                <input class="form-check-input" name="check[]" type="checkbox" value="TettuccioApribile" id="flexSwitchCheckDefault">
                <label class="form-check-label" for="flexSwitchCheckDefault"><b>Tettuccio Apribile</b></label>
                <br>
                <input class="form-check-input" name="check[]" type="checkbox" value="CruiseControl" id="flexSwitchCheckDefault">
                <label class="form-check-label" for="flexSwitchCheckDefault"><b>Cruise Control</b></label>
                <br>
                <input class="form-check-input" name="check[]" type="checkbox" value="AppleCar_e_Android_auto" id="flexSwitchCheckDefault">
                <label class="form-check-label" for="flexSwitchCheckDefault"><b>Apple CarPlay </b></label>
                <br>
                <input class="form-check-input" name="check[]" type="checkbox" value="Sensori_di_parcheggio" id="flexSwitchCheckDefault">
                <label class="form-check-label" for="flexSwitchCheckDefault"><b>Sensori di Parcheggio</b></label>
                <br>
                </div>
            </div>




            <br><br>

            <button type="submit" class="btn btn-primary" name="modifica">Salva nel carrello</button><br>

                @if($errors->first('tipoAlimentazione')||$errors->first('nPorte')||$errors->first('tipoCambio'))

                         <p><b>*Configurazioni Obbligatorie</b></p>
                @endif

        </div>

    </form>
@endsection
