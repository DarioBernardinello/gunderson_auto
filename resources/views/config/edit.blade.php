@extends('templates.default')
<?php
    use App\Models\Prezzi;
?>
@section('content')

    <br>
    <form action="/config/{{$config->id}}" style="text-align:center" method="POST">
        <h1>Modifica </h1><br><br>
        <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
        <input type="hidden" name="_method" value="PATCH">
        <div class="form-group">
            <label for="stato"><b>Stato Configurazione</b></label><br>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="stato" id="inlineRadio1" @if($config->stato=='privato') checked @endif value="privato">
                <label class="form-check-label" for="inlineRadio1">Privata</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="stato" id="inlineRadio2" @if($config->stato=='pubblico') checked @endif value="pubblico">
                <label class="form-check-label" for="inlineRadio2">Pubblica</label>
            </div><br><br>
        <div class="form-group">

            <label for="tipoAlimentazione"><b>*Alimentazione</b></label><br>

            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="tipoAlimentazione" id="inlineRadio1" @if($config->tipoAlimentazione=='Benzina') checked @endif value="Benzina">
                <label class="form-check-label" for="inlineRadio1">Benzina</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="tipoAlimentazione" id="inlineRadio2" @if($config->tipoAlimentazione=='Diesel') checked @endif value="Diesel">
                <label class="form-check-label" for="inlineRadio2">Diesel</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="tipoAlimentazione" id="inlineRadio3" @if($config->tipoAlimentazione=='Elettrico') checked @endif value="Elettrico" >
                <label class="form-check-label" for="inlineRadio3">Elettrico</label>
            </div>

        </div>
        <br>

        <div class="form-group">
            <label for="nPorte"><b>*Numero Porte</b></label><br>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="nPorte" id="inlineRadio1" @if($config->nPorte=='3') checked @endif value="3">
                <label class="form-check-label" for="inlineRadio1">3 </label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="nPorte" id="inlineRadio2" @if($config->nPorte=='5') checked @endif value="5">
                <label class="form-check-label" for="inlineRadio2">5</label>
            </div>

        </div>

        <br>

        <div class="form-group">
            <label for="tipoCambio"><b>*Tipo di Cambio</b></label><br>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="tipoCambio" id="inlineRadio1" @if($config->tipoCambio=='manuale') checked @endif value="manuale">
                <label class="form-check-label" for="inlineRadio1">Manuale</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="tipoCambio" id="inlineRadio2" @if($config->tipoCambio=='automatico') checked @endif value="automatico">
                <label class="form-check-label" for="inlineRadio2">Automatico</label>
            </div>

        </div>
        <br>
        <h2>Optional</h2><br>
        <div class="form-group">
            @if(isset($optional))
                @foreach($optional as $o)
                   <?php
                    $colore=$o->colore;

                       $array=explode(" ,", $o->check);

                    ?>
                @endforeach
            @endif
            <label for="exampleColorInput" class="form-label"><b>Scegli il colore della tua macchina:</b></label>
            <input type="color" style="margin-left:auto;margin-right:auto;"class="form-control form-control-color" name="colore" id="exampleColorInput" value="@if(isset($colore)){{old('colore', $colore)}}@endif" title="Choose your color">
            <br>
            <div class="form-check form-switch">
                <h4 style="color:#ff0000;">Elenco Optional</h4>
                <input class="form-check-input inline-block" name="check[]" type="checkbox" value="Cerchi_in_lega" id="flexSwitchCheckDefault" @if(isset($array)) @if(in_array("Cerchi_in_lega",$array)) checked @endif @endif>
                <label class="form-check-label" for="flexSwitchCheckDefault"><b>Cerchi in lega</b></label>
                <br>
                <input class="form-check-input" name="check[]" type="checkbox" value="VetriOscurati" id="flexSwitchCheckDefault"@if(isset($array)) @if(in_array("VetriOscurati",$array)) checked @endif @endif>
                <label class="form-check-label" for="flexSwitchCheckDefault"><b>Vetri Oscurati</b></label>
                <br>
                <input class="form-check-input" name="check[]" type="checkbox" value="Fari_a_led" id="flexSwitchCheckDefault" @if(isset($array)) @if(in_array("Fari_a_led",$array)) checked @endif @endif>
                <label class="form-check-label" for="flexSwitchCheckDefault"><b>Fari full-led</b></label>
                <br>
                <input class="form-check-input" name="check[]" type="checkbox" value="TettuccioApribile" id="flexSwitchCheckDefault" @if(isset($array)) @if(in_array("TettuccioApribile",$array)) checked @endif @endif>
                <label class="form-check-label" for="flexSwitchCheckDefault"><b>Tettuccio Apribile</b></label>
                <br>
                <input class="form-check-input" name="check[]" type="checkbox" value="CruiseControl" id="flexSwitchCheckDefault @if(isset($array))"@if(in_array("CruiseControl",$array)) checked @endif @endif>
                <label class="form-check-label" for="flexSwitchCheckDefault"><b>Cruise Control</b></label>
                <br>
                <input class="form-check-input" name="check[]" type="checkbox" value="AppleCar_e_Android_auto" id="flexSwitchCheckDefault" @if(isset($array))@if(in_array("AppleCar_e_Android_auto",$array)) checked @endif @endif>
                <label class="form-check-label" for="flexSwitchCheckDefault"><b>Apple CarPlay </b></label>
                <br>
                <input class="form-check-input" name="check[]" type="checkbox" value="Sensori_di_parcheggio" id="flexSwitchCheckDefault" @if(isset($array)) @if(in_array("Sensori_di_parcheggio",$array)) checked @endif @endif>
                <label class="form-check-label" for="flexSwitchCheckDefault"><b>Sensori di Parcheggio</b></label>
                <br>
            </div>






            <br>
            <button type="submit" class="btn btn-primary" name="modifica">Salva Cambiamenti</button>
                @if($errors->first('tipoAlimentazione')||$errors->first('nPorte')||$errors->first('tipoCambio'))
                     <p><b>*Configurazioni Obbligatorie</b></p>
                @endif
            </form>



        </div>


@endsection
