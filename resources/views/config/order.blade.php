@extends('templates.default')

@section('content')


    <form action="/cart/store/{{$config->id}}" method="POST" style="text-align:center">
        <h1>Ordina la configurazione N. {{$config->id}}</h1><br>
        <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
        <input type="hidden" name="_method" value="PATCH">
        <div class="form-group">
            <table class="table table-striped center" style="width:50%;margin-left:auto;margin-right:auto;">
                <tr>
                <tr><td style="text-align:center;"colspan="2"><b>Configurazioni presenti</b></td></tr>
                </tr>
                <tr>
                    <td><b>Tipo Alimentazione</b></td>
                    <td>{{$config->tipoAlimentazione}}</td>
                </tr>
                <tr>
                    <td><b>N. Porte</b></td>
                    <td>{{$config->nPorte}}</td>
                </tr>
                <tr>
                    <td><b>Tipo Cambio</b></td>
                    <td>{{$config->tipoCambio}}</td>
                </tr>
                @if(isset($optional))
                    @foreach($optional as $o)
                       <?php
                        $colore=$o->colore;
                        $costo=0;
                        $iva=1.22;
                        $array=explode(" ,", $o->check);?>
                    @endforeach
                        <?php
                        foreach($prezzi as $prezzo){
                            if($prezzo->campo=="CostoTotale"){
                                $costo=$costo+$prezzo->prezzo;
                            }
                            if($prezzo->campo=="Benzina" && $config->tipoAlimentazione=="Benzina"){
                                $costo=$costo+$prezzo->prezzo;
                            }
                            if($colore!="#000000" && $prezzo->campo=="Benzina"){
                                $costo=$costo+$prezzo->prezzo;
                            }
                            if($prezzo->campo=="Diesel" && $config->tipoAlimentazione=="Diesel"){
                                $costo=$costo+$prezzo->prezzo;
                            }
                            if($prezzo->campo=="Elettrico" && $config->tipoAlimentazione=="Elettrico"){
                                $costo=$costo+$prezzo->prezzo;
                            }
                            if($prezzo->campo=="3" && $config->nPorte=="3"){
                                $costo=$costo+$prezzo->prezzo;
                            }
                            if($prezzo->campo=="5" && $config->nPorte=="5"){
                                $costo=$costo+$prezzo->prezzo;
                            }
                            if($prezzo->campo=="manuale" && $config->tipoCambio=="manuale"){
                                $costo=$costo+$prezzo->prezzo;
                            }
                            if($prezzo->campo=="automatico" && $config->tipoCambio=="automatico"){
                                $costo=$costo+$prezzo->prezzo;
                            }
                            if(isset($array)){
                                if(in_array("Cerchi_in_lega",$array)&&$prezzo->campo=="Cerchi_in_lega"){
                                    $costo=$costo+$prezzo->prezzo;
                                }
                                if(in_array("VetriOscurati",$array)&&$prezzo->campo=="VetriOscurati"){
                                    $costo=$costo+$prezzo->prezzo;
                                }
                                if(in_array("Fari_a_led",$array)&&$prezzo->campo=="Fari_a_led"){
                                    $costo=$costo+$prezzo->prezzo;
                                }
                                if(in_array("VetriOscurati",$array)&&$prezzo->campo=="VetriOscurati"){
                                    $costo=$costo+$prezzo->prezzo;
                                }
                                if(in_array("TettuccioApribile",$array)&&$prezzo->campo=="TettuccioApribile"){
                                    $costo=$costo+$prezzo->prezzo;
                                }
                                if(in_array("CruiseControl",$array)&&$prezzo->campo=="CruiseControl"){
                                    $costo=$costo+$prezzo->prezzo;
                                }
                                if(in_array("AppleCar_e_Android_auto",$array)&&$prezzo->campo=="AppleCar_e_Android_auto"){
                                    $costo=$costo+$prezzo->prezzo;
                                }
                                if(in_array("Sensori_di_parcheggio",$array)&&$prezzo->campo=="Sensori_di_parcheggio"){
                                    $costo=$costo+$prezzo->prezzo;
                                }
                            }
                        }
                        ?>
                @endif

               <tr><td style="text-align:center;"colspan="2"><b>Optional presenti</b></td></tr>
                <tr>
                    <td><b>Cambio Colore</b></td>
                    <td>@if($colore=='#000000') NON @endif PRESENTE</td>
                </tr>
                <tr>
                    <td><b>Cerchi in lega</b></td>
                    <td>@if(isset($array)) @if(!in_array("Cerchi_in_lega",$array)) NON  @endif  PRESENTE  @endif  </td>
                </tr>
                <tr>
                    <td><b>Vetri Oscurati</b></td>
                    <td>@if(isset($array)) @if(!in_array("VetriOscurati",$array)) NON @endif PRESENTE @endif </td>
                </tr>
                <tr>
                    <td><b>Fari a led</b></td>
                    <td>@if(isset($array)) @if(!in_array("Fari_a_led",$array)) NON @endif PRESENTE @endif</td>
                </tr>
                <tr>
                    <td><b>Tettuccio Apribile</b></td>
                    <td>@if(isset($array)) @if(!in_array("TettuccioApribile",$array)) NON @endif PRESENTE @endif</td>
                </tr>
                <tr>
                    <td><b>Cruise Control</b></td>
                    <td>@if(isset($array)) @if(!in_array("CruiseControl",$array)) NON @endif PRESENTE @endif</td>
                </tr>
                <tr>
                    <td><b>Apple Car & Android Auto</b></td>
                    <td>@if(isset($array)) @if(!in_array("AppleCar_e_Android_auto",$array)) NON @endif PRESENTE @endif</td>
                </tr>
                <tr>
                    <td><b>Sensori di Parcheggio</b></td>
                    <td> @if(isset($array)) @if(!in_array("Sensori_di_parcheggio",$array)) NON @endif PRESENTE @endif</td>
                </tr>


            </table><br>
            <h5>Costo Totale: {{$costo}}&#8364</h5>

            <h5>Prezzo+iva: {{ceil($costo*$iva)}}&#8364</h5><br>
           <button type="submit" class="btn btn-danger" name="ordina">Ordina</button></a>
            <a id="update-button" href="/config/{{$config->id}}/edit" class="btn btn-success">Update</a>
        </div>


</form>
@endsection
