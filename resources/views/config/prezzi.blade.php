@extends('templates.default')

@section('content')
    <h1>Listino dei prezzi</h1><br><br>
    <table class="table table-striped table-hover">


        <tr>
            <td><b>Tipo</b></td>
            <td><b>Prezzo</b></td>
        </tr>
        @foreach($prezzi as $prezzo)
        <tr>
            <td>{{$prezzo->campo}}</td>
            <td>{{$prezzo->prezzo}} &#8364</td>


        </tr>
        @endforeach
        <tr style="color:red">
            <td><b>Iva</b></td>
            <td colspan=2><b>+22%</b></td>

        </tr>
    </table>


@endsection
