<section class="footer">

    <footer class="text-center text-white" style="background-color: red;">
        @if(Auth::guest())
        <div class="container p-4">

      <section class="">
        <p class="d-flex justify-content-center align-items-center">
          <span class="me-3">Registrati Gratis</span>
          <a href="{{route('register')}}"button type="button" class="btn btn-outline-light btn-rounded">
            Registrati
          </button> </a>
        </p>
      </section>

    </div>
        @endif
        <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">

            Copyright By Ibiza &copy 2021 Author: Dario Bernardinello
        </div>
    </footer>
</section>
