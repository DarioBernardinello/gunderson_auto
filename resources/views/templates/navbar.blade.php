<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="/"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>



    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">

                <a class="nav-link" href="{{route('config')}}">Home <span class="sr-only">(current)</span></a>
            </li>
            @if(Auth::check())
                <li class="nav-item">
                    <a class="nav-link" href="{{route('config.create')}}">New Config</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="{{route('config.cart')}}">Cart</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="{{route('config.publicConfig')}}">Public Config</a>
                </li>

                    <li class="nav-item">
                        <a class="nav-link" href="{{route('config.prezzi')}}">Prezzi</a>
                    </li>

            @endif
        </ul>



        <ul class="nav navbar-nav navbar-right">
            <!-- Authentication Links -->
            @if (Auth::guest())
                <li><a class="nav-link" href="{{ url('/login') }}">Login</a></li>
                <li><a class="nav-link" href="{{ url('/register') }}">Register</a></li>
            @else

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle  nav-link " data-toggle="dropdown" role="button" aria-expanded="false">
                        Hello @if(Auth::user()->role=='admin') Admin @endif  {{Auth::user()->username}} <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu" role="menu">
                        @if(Auth::user()->isAdmin())
                            <li>
                                <a href="{{route('admin.configs')}}">Admin</a>
                            </li>

                        @endif
                        <li>
                            <a href="{{ url('/logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>




                        </li>

                    </ul>
                </li>
            @endif
        </ul>

    </div>
</nav>
