<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ConfigController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\PrezziController;
use App\Http\Controllers\AdminController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');



Route::get('/termini', function(){
    return view('auth/termini');
});

Route::group(['middleware' => 'auth'], function () {
    //Pagina iniziale dove l'utente vede le proprie configurazioni
    Route::get('/', [ConfigController::class, 'index'])->name('config');
    Route::get('/home', [ConfigController::class, 'index'])->name('config');

    //L'utente vede i prezzi di ogni configurazione e i vari optional
    Route::get('/prezzi',[PrezziController::class,'index'])->name('config.prezzi');

    //L'utente vede tutte le configurazioni pubbliche
    Route::get('/PublicConfig',[ConfigController::class,'publicConfig'])->name('config.publicConfig');


    Route::prefix('/config')->group(function(){
        //L'utente elimina la configurazione selezionata
        Route::get('/{config}',[ConfigController::class, 'destroy'])->name('config.destroy')->where('config', '[0-9]+');

        //L'utente crea una nuova configurazione
        Route::get('/create', [ConfigController::class, 'create'])->name('config.create');

        //L'utente aggiorna una configurazione già creata
        Route::get('/{id}/edit',[ConfigController::class, 'edit']);
        Route::patch('/{id}',[ConfigController::class, 'store']);
        Route::post('/',[ConfigController::class, 'save'])->name('config.save');});


    Route::prefix('/cart')->group(function(){

        //L'utente vede il proprio carrello con tutte le configurazioni, incluse quelle ordinate
        Route::get('/', [ConfigController::class, 'cart'])->name('config.cart');

        //L'utente ordina la configurazione
        Route::post('/order/{id}', [CartController::class, 'cartOrder'])->name('cart.order');

        //L'utente modifica la configurazione
        Route::get('/{id}/edit', [CartController::class, 'cartEdit']);
        Route::patch('/store/{id}', [CartController::class, 'store']);
    });
});







Route::group(['middleware'=>['auth','admin']],function(){
    Route::get('/dashboard',function(){
        return view('admin.dashboard')->name('admin.dashboard');
    });
    //L' Admin visualizza e modifica i vari prezzi
    Route::get('/Admin',[PrezziController::class,'show'])->name('admin.Admin');

    //L' Admin modifica i prezzi
    Route::get('prezzi/{id}/edit',[PrezziController::class, 'edit']);

    //L' Admin Salva i prezzi modificati
    Route::patch('/prezzi/{id}', [PrezziController::class, 'store']);

    //L' Admin visualizza tutte le configurazioni
    Route::get('/Admin/Configs',[AdminController::class,'show'])->name('admin.configs');

    //L' Admin visualizza tutti gli utenti registrati e i loro privilegi
    Route::get('/users', [AdminController::class,'index'])->name('admin.user');

    //L'Admin modifica l'utente selezionato
    Route::get('users/{id}/edit',[AdminController::class,'edit']);
    Route::patch('/users/{id}', [AdminController::class, 'store']);

    //L'Admin elimina l'utente selezionato
    Route::get('users/{user}',[AdminController::class, 'destroy'])->name('user.destroy')->where('config', '[0-9]+');
    Route::view('/','admin/dashboard')->name('dashboard');
});

require __DIR__.'/auth.php';
